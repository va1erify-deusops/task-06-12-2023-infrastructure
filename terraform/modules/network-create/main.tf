resource "yandex_vpc_network" "network" {
  name = var.name_network
}

resource "yandex_vpc_subnet" "subnet" {
  name           = var.name_subnetwork
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = [var.cidr_v4]
  zone           = var.zone
}
