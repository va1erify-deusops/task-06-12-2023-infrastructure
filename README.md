## Gitlab CI/CD vars

| Type     | Key         | Description               |
|----------|-------------|---------------------------|
| Variable | CLOUD_ID     | Masked Yandex cloud ID. Used as a variable in the Terraform pipeline to identify the Yandex Cloud ID.          |
| Variable | FOLDER_ID    | Masked Yandex folder ID. Used as a variable in the Terraform pipeline to specify the target folder in Yandex Cloud. |
| Variable | TOKEN        | Masked Yandex token. Used as a variable in the Terraform pipeline to authenticate with the Yandex Cloud API.       |


# Terraform CI/CD Pipeline

This repository contains the Terraform code for provisioning and managing infrastructure. The CI/CD pipeline includes the following stages:

## Stages

### 1. Terraform Validate

- **Description:** Validates the Terraform configuration files.
- **Command:** `terraform validate`
- **Status:** Automatically triggered.

### 2. Terraform Plan

- **Description:** Generates an execution plan for the Terraform configuration.
- **Command:** `terraform plan -var cloud_id=$CLOUD_ID -var folder_id=$FOLDER_ID -var token=$TOKEN -out="planfile"`
- **Status:** Automatically triggered.

### 3. Terraform Apply (Manual)

- **Description:** Applies the Terraform execution plan to create or update infrastructure.
- **Command:** `terraform apply -auto-approve "planfile"`
- **Status:** Manual trigger required.

### 4. Terraform Destroy (Manual)

- **Description:** Destroys the provisioned infrastructure.
- **Command:** `terraform destroy -var cloud_id=$CLOUD_ID -var folder_id=$FOLDER_ID -var token=$TOKEN -auto-approve`
- **Status:** Manual trigger required.

## Terraform State Storage

To store Terraform state, caching is utilized. The cache includes the following directories and files:

- `terraform/.terraform`
- `terraform/planfile`
- `terraform/terraform.tfstate`
- `terraform/terraform.tfstate.backup`
- `terraform/.terraform.lock.hcl`
- `terraform/.terraform.tfstate.lock.info`

These elements are necessary to persist the infrastructure state between pipeline runs.



## Terraform Initialization and Configuration

Before each Terraform execution, the pipeline performs the following steps:

```bash
before_script:
  - cat terraform/terraformrc.config > ~/.terraformrc
  - cd terraform
  - terraform init
```

## Running the Pipeline

To run the pipeline, follow these steps:

1. Push your changes to the repository.
2. Navigate to the CI/CD section on GitLab.
3. Manually trigger the pipeline by selecting the desired pipeline stage:
   - To apply Terraform changes, trigger the `terraform-apply` stage.
   - To destroy infrastructure, trigger the `terraform-destroy` stage.
